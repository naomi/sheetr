import './style.css'

import { main } from './src/sheetr.gleam'

document.addEventListener("DOMContentLoaded", () => {
    const dispatch = main({});
})
