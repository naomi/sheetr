import gleam/dynamic
import gleam/float
import gleam/int
import gleam/list
import gleam/result
import gleam/string
import glormat as fmt
import lustre
import lustre/attribute.{type Attribute} as attr
import lustre/effect.{type Effect}
import lustre/element.{type Element}
import lustre/element/html
import lustre/event
import prng/random

pub type Stat =
  #(String, String, Int)

pub type Stats =
  List(Stat)

pub type Feature =
  #(Int, String, String)

pub type Class {
  Class(name: String, hit_die: Int, features: List(Feature))
}

pub type Character {
  Character(name: String, stats: Stats, classes: List(#(Int, Class)))
}

pub type RollType {
  Advantage
  Disadvantage
  Normal
  None
}

pub type Roll {
  Roll(die: Int, roll: Int, mod: Int, roll2: Int, variant: RollType)
}

pub type Model {
  Model(character: Character, current_health: Int, last_roll: Roll)
}

pub type Msg {
  IncrementHealth
  DecrementHealth
  RollDice(die: Int, mod: Int, variant: RollType)
}

pub fn main() {
  let app = lustre.application(init, update, view)
  let assert Ok(dispatch) = lustre.start(app, "#app", Nil)

  dispatch
}

fn fighter() -> Class {
  Class(name: "Fighter", hit_die: 10, features: [
    #(5, "Extra Attack", "Gives an extra attack"),
  ])
}

fn barbarian() -> Class {
  Class(name: "Barbarian", hit_die: 12, features: [])
}

fn test_character() -> Character {
  Character(
    name: "Test Character",
    stats: [
      #("Strength", "str", 17),
      #("Dexterity", "dex", 13),
      #("Constitusion", "con", 16),
      #("Intelligence", "int", 8),
      #("Wisdom", "wis", 10),
      #("Charisma", "cha", 11),
    ],
    classes: [#(10, fighter()), #(10, barbarian())],
  )
}

fn init(_) -> #(Model, Effect(Msg)) {
  #(Model(test_character(), 0, Roll(0, 0, 0, 0, None)), effect.none())
}

fn update(model: Model, msg: Msg) -> #(Model, Effect(Msg)) {
  case msg {
    IncrementHealth -> #(
      Model(..model, current_health: model.current_health + 1),
      effect.none(),
    )
    DecrementHealth -> #(
      Model(..model, current_health: model.current_health - 1),
      effect.none(),
    )
    RollDice(die, mod, var) -> #(
      Model(..model, last_roll: roll_die(die, mod, var)),
      effect.none(),
    )
  }
}

fn roll_die(die: Int, mod: Int, variant: RollType) -> Roll {
  let generator = random.int(1, die)
  case variant {
    Normal -> Roll(die, random.random_sample(generator), mod, 0, Normal)
    Advantage ->
      Roll(
        die,
        random.random_sample(generator),
        mod,
        random.random_sample(generator),
        Advantage,
      )
    Disadvantage ->
      Roll(
        die,
        random.random_sample(generator),
        mod,
        random.random_sample(generator),
        Disadvantage,
      )
    None -> Roll(0, 0, 0, 0, None)
  }
}

fn stat_block(stat: #(String, String, Int)) -> Element(Msg) {
  let score = int.to_string(stat.2)
  let modifier = { stat.2 - 10 } / 2
  let mod = case modifier {
    v if v > 0 -> "+" <> int.to_string(v)
    v -> int.to_string(v)
  }
  html.div([attr.class("stat-block")], [
    html.h2([], [
      element.text(
        stat.1
        |> string.uppercase,
      ),
    ]),
    html.h2([], [element.text(score)]),
    html.button(
      [
        on_variant_click(
          RollDice(20, modifier, Normal),
          RollDice(20, modifier, Disadvantage),
          RollDice(20, modifier, Advantage),
        ),
      ],
      [element.text(mod)],
    ),
  ])
}

fn on_variant_click(normal: msg, shift: msg, ctrl: msg) -> Attribute(msg) {
  use event <- event.on("click")
  use shift_key_pressed: Bool <- result.try(
    event
    |> dynamic.field("shiftKey", dynamic.bool),
  )
  use ctrl_key_pressed: Bool <- result.try(
    event
    |> dynamic.field("ctrlKey", dynamic.bool),
  )

  Ok(case shift_key_pressed, ctrl_key_pressed {
    True, False -> shift
    False, True -> ctrl
    _, _ -> normal
  })
}

fn character(model: Model) -> Element(Msg) {
  let character = model.character
  let current_health = int.to_string(model.current_health)
  let assert Ok(con) =
    character.stats
    |> list.drop(2)
    |> list.first
  let modifier = { con.2 - 10 } / 2
  let max_health =
    model.character.classes
    |> list.map(fn(class: #(Int, Class)) {
      calculate_average_hit_points(class.0, { class.1 }.hit_die, modifier)
    })
    |> int.sum
    |> int.to_string

  html.div([], [
    html.h1([], [element.text(character.name)]),
    html.div(
      [attr.class("stat-container")],
      character.stats
        |> list.map(stat_block),
    ),
    html.p([attr.class("text-center")], [
      element.text("Health: " <> current_health <> "/" <> max_health),
    ]),
    html.div([attr.class("text-center")], [
      html.button([event.on_click(DecrementHealth)], [element.text("-")]),
      html.button([event.on_click(IncrementHealth)], [element.text("+")]),
    ]),
  ])
}

fn roll_bar(roll: Roll) -> Element(Msg) {
  let die = int.to_string(roll.die)
  let mod = int.to_string(roll.mod)
  let roll1 = int.to_string(roll.roll)
  let res1 = int.to_string(roll.roll + roll.mod)
  let roll2 = int.to_string(roll.roll2)
  let res2 = int.to_string(roll.roll2 + roll.mod)

  html.div([attr.class("roll-bar")], case roll.variant {
    None -> []
    Normal -> [
      element.text(
        "d{die}: {roll} + {mod} = {res}"
        |> fmt.replace("die", with: die)
        |> fmt.then("roll", with: roll1)
        |> fmt.then("mod", with: mod)
        |> fmt.then("res", with: res1)
        |> fmt.assert_ok,
      ),
      html.button([event.on_click(RollDice(0, 0, None))], [
        element.text("Dismiss"),
      ]),
    ]
    Advantage -> [
      element.text(
        "d{die} (Advantage): {roll1} + {mod} = {res1} | {roll2} + {mod} = {res2}"
        |> fmt.replace("die", with: die)
        |> fmt.then("roll1", with: roll1)
        |> fmt.then("mod", with: mod)
        |> fmt.then("res1", with: res1)
        |> fmt.then("roll2", with: roll2)
        |> fmt.then("res2", with: res2)
        |> fmt.assert_ok,
      ),
      html.button([event.on_click(RollDice(0, 0, None))], [
        element.text("Dismiss"),
      ]),
    ]
    Disadvantage -> [
      element.text(
        "d{die} (Disadvantage): {roll1} + {mod} = {res1} | {roll2} + {mod} = {res2}"
        |> fmt.replace("die", with: die)
        |> fmt.then("roll1", with: roll1)
        |> fmt.then("mod", with: mod)
        |> fmt.then("res1", with: res1)
        |> fmt.then("roll2", with: roll2)
        |> fmt.then("res2", with: res2)
        |> fmt.assert_ok,
      ),
      html.button([event.on_click(RollDice(0, 0, None))], [
        element.text("Dismiss"),
      ]),
    ]
  })
}

fn view(model: Model) -> Element(Msg) {
  set_character_as_title(model.character)
  html.div([], [roll_bar(model.last_roll), character(model)])
}

@external(javascript, "./js_ffi.js", "set_title")
fn set_title(title: String) -> Nil

fn set_character_as_title(character: Character) -> Nil {
  set_title("Sheetr - " <> character.name)
}

fn calculate_average_hit_points(level: Int, hit_die: Int, modifer: Int) -> Int {
  let level = int.to_float(level - 1)
  let hit_die = int.to_float(hit_die)
  let modifer = int.to_float(modifer)
  let per_level =
    { hit_die /. 2.0 +. 0.5 }
    |> float.round
    |> int.to_float
  { hit_die +. modifer } +. level *. per_level +. { level *. modifer }
  |> float.round
}
