import { EventsOn, WindowSetTitle } from "../wailsjs/runtime/runtime";
// import { Ok, Error } from "./gleam.mjs";

export function listenForTick(handler) {
  return EventsOn('app:tick', (data) => {
    handler(data)
  })
}

export function from_unix(timestamp) {
  return new Date(timestamp * 1000)
}

export function date_to_string(date) {
  return date;
}

export function set_title(title) {
  WindowSetTitle(title)
}